﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s9_oop_lab3.BinarySearchTree
{
    interface IBinarySearchTreeNode<T> : IComparable
        where T : IComparable<T>
    {
        public T Key { get; }
        public IBinarySearchTreeNode<T> Left { get; set; }
        public IBinarySearchTreeNode<T> Right { get; set; }
    }
}
