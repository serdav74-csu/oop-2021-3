﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s9_oop_lab3.BinarySearchTree
{
    class BinarySearchTreeNode<T> : IBinarySearchTreeNode<T>
        where T: IComparable<T>
    {
        public T Key { get; }
        public IBinarySearchTreeNode<T> Left { get; set; }
        public IBinarySearchTreeNode<T> Right { get; set; }
        public int CompareTo(object other)
        {
            if (other == null) return 1;

            if (other is IBinarySearchTreeNode<T> other_node)
            {
                return Key.CompareTo(other_node.Key);
            }
            else
            {
                throw new ArgumentException("Cannot compare to this type!");
            }
        }
    }
}
