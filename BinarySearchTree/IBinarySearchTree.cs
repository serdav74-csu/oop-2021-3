﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s9_oop_lab3.BinarySearchTree
{
    interface IBinarySearchTree<T, Node>
        where T: IComparable<T>
        where Node: IBinarySearchTreeNode<T>
    {
        public Node Find(T key);
        public void Insert(T key);
        public void Remove(T key);
    }
}
